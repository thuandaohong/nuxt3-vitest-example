import { describe, expect, it, test } from "vitest"
import { setup, $fetch } from "@nuxt/test-utils-edge"
import { mount } from "@vue/test-utils"
import app from "./app.vue"

describe("My test", async () => {
  await setup({
    // test context options
  })

  it("my test", async () => {
    const wrapper = mount(app)

    expect(+wrapper.find('[data-testid="number"]').text()).toEqual(0)

    await wrapper.find('[data-testid="increase"').trigger("click")

    expect(+wrapper.find('[data-testid="number"]').text()).toEqual(1)
  })
})
